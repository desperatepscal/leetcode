import java.util.*;
class Solution {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int i = g.length - 1;
        int j = s.length - 1;
        int val = 0;
        while (i >=0 && j >= 0) {
            if (g[i] <= s[j]) {
                val++;
                j--;
            }
            i--;
        }
        return val;
    }
}

class Test {
    public static void main(String args[]) {
        int[] g1 = new int[]{3,1,2,1};
        int[] g2 = new int[]{3, 4, 5};

        int[] s1 = new int[]{1,1};
        int[] s2 = new int[]{6, 7, 8};

        Solution s = new Solution();
        System.out.println(s.findContentChildren(g1, s1));
        System.out.println(s.findContentChildren(g2, s2));
    }
}