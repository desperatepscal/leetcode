class Solution {
    public int numFriendRequests(int[] ages) {
        int[] count = new int[121];
        int countA;
        int countB;
        int val = 0;
        for (int p: ages) count[p]++;
        for (int i = 0; i < 121; i++) {
            countA = count[i];
            for (int j = 0; j < 121; j++) {
                countB = count[j];
                if ((i * 0.5 + 7 >= j)
                    || (i < j)
                    || (i < 100 && j > 100)) continue;
                val += countA * countB;
                if (i == j) val -= countA;
            }
        }
        return val;
    }
}

class Test {
    public static void main(String args[]) {
        Solution s = new Solution();
        int[] a1 = new int[]{16, 17, 18};
        int[] a2 = new int[]{16, 16};
        System.out.println(s.numFriendRequests(a1));
        System.out.println(s.numFriendRequests(a2));
    }
}