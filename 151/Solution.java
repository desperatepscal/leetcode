class Solution {
    static String reverseWords(String s){
        StringBuilder val = new StringBuilder();
        String[] splited = s.split("\\s+");
        for (int i = splited.length - 1; i >=0; i--) {
            val.append(splited[i]);
            val.append(" ");
        }
        return val.toString().trim();
    }
    public static void main(String args[]){
        System.out.println(reverseWords("a simple sentence"));
    }
}