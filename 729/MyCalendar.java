import java.util.*;
class MyCalendar {
    List<int[]> calendar;
    public MyCalendar() {
        calendar = new ArrayList<int[]>();
    }
    public boolean book(int start, int end) {
        int i = 0;
        while (i < calendar.size() && calendar.get(i)[1] <= start) i++;
        if (i < calendar.size() && calendar.get(i)[0] < end) return false;
        calendar.add(i, new int[]{start, end});
        return true;
    }
}

class Test {
    public static void main(String args[]) {
        MyCalendar obj = new MyCalendar();
        System.out.println(obj.book(10, 20));
        System.out.println(obj.book(15, 20));
        System.out.println(obj.book(20, 25));
    }
}