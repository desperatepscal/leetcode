import java.util.*;
class Solution {
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> arr = new ArrayList<Integer>();
        int abs_i;
        for (int i = 0; i < nums.length; i++) {
            abs_i = Math.abs(nums[i]) - 1;
            if (nums[abs_i] < 0) arr.add(abs_i + 1);
            nums[abs_i] = - nums[abs_i];
        }
        return arr;
    }
}

class Test {
    public static void main(String args[]) {
        int[] i1 = new int[]{4,3,2,7,8,2,3,1};
        Solution s = new Solution();
        System.out.println(s.findDuplicates(i1));
    }
}