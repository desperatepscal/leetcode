class Solution {
    int[] stack;
    int[][][] dp;
    public int removeBoxes(int[] boxes) {
        stack = boxes;
        dp = new int[boxes.length][boxes.length][boxes.length];
        return dfs(0, boxes.length - 1, 0);
    }
    public int dfs(int l, int r, int k) {
        if (l > r) return 0;
        while (l < r && stack[r] == stack[r - 1]) {
            r--;
            k++;
        }
        if (dp[l][r][k] > 0) return dp[l][r][k];
        dp[l][r][k] = dfs(l, r - 1, 0) + (k + 1) * (k + 1);
        for (int i = l; i < r; i++) {
            if (stack[i] == stack[r]) {
                dp[l][r][k] = Math.max(dp[l][r][k], dfs(l, i, k + 1) + dfs(i + 1, r - 1, 0));
            }
        }
        return dp[l][r][k];
    }
}

class Test {
    public static void main(String args[]) {
        int[] list1 = new int[]{1,1,1,1,1,1};
        int[] list2 = new int[]{1,2,3,2,4,1};
        Solution s = new Solution();
        System.out.println(list1 + ": " + s.removeBoxes(list1));
        System.out.println(list2 + ": " + s.removeBoxes(list2));
    }
}