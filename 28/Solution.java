class Solution {
    static int strStr(String haystack, String needle) {
        if (needle.isEmpty()) {
            return 0;
        }
        for (int i = 0; i < haystack.length(); i++) {
            if(haystack.charAt(i) == needle.charAt(0)
                && haystack.length() - i >= needle.length()) {
                    if (haystack.substring(i, i + needle.length()).equals(needle)) {
                        return i;
                    }
            }
        }
        return -1;
    }
    public static void main(String args[]) {
        String s_hay = "githubomgff";
        String s_nee = "bom";
        System.out.println(strStr("", ""));
        System.out.println(strStr(s_hay, s_nee));
    }
}