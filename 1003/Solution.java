import java.util.*;
class Solution {
    public boolean isValid(String S) {
        Stack<Character> stack = new Stack<Character>();
        for(char curr: S.toCharArray()) {
            if(curr == 'a') {
                stack.push('c');
                stack.push('b');
            }
            else if (stack.isEmpty() || stack.pop() != curr) {
                return false;
            }
        }
        return stack.isEmpty();
    }
}


class Test {
    public static void main(String args[]) {
        String[] l = new String[]{
            "aabcbc",
            "abcabcababcc",
            "abccba",
            "cababc"
        };
        Solution s = new Solution();

        for (String i : l) {
            System.out.println(s + ": " + s.isValid(i));
        }
    }
}